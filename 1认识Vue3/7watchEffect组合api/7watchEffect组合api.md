# watchEffect(回调函数)
## 回调函数调用的时机：
    一上来就调用
    回调函数中所用到的响应式数据变化的时候被调用
## 区别于watch(监视的对象，回调函数，配置项)
    例： 
    1、若监视ref'定义的数值类型数据
    let sum=ref(0)
    watch(sum/()=>sum.value,handler,{})  【//注】不能够sum.value，因为是一个值，同时也不需要deep，因为数值没有层次
    2、若监视ref'定义的对象类型数据
    let person=ref({
        name:'jane',
        age:{
            realAge:34,
            virtualAge:22
        }
    })
    watch(person,handler,{deep:true})/watch(person.value,handler,{}),才能监视person中的变化，因为person是一个refimpl的引用对象，person.value是借助reactive()函数生成的proxy类型对象，那么由reactive定义的数据已经开启了深度监视，但是refimpl没有开启深度监视，所以如果直接监测ref定义的对象的时候，需要配置deep：true

    3、利用watchEffect(handler)来监视数据，类似于计算属性，他的调用时机和watchEffect的回调调用时机是一样的
