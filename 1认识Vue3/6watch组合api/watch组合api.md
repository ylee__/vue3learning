# 组合api就是一堆内置的函数
    通过引入组合api import {ref,computed,reactive,watch}
    ref()/reactive()/computed()/watch()
# watch组合api
## 情况一 如果监视的是ref定义的基本数值类型的refimpl引用对象
   >+ 如果监视一个属性   
    例：let msg=ref('你好啊')、let sum=ref(0)
    则watch(监视的数据，(newValue,oldValue)=>{之前vue2中的handler函数},{immediate:true,deep:true等配置项})
   >+ 如果监视多个属性
   1、写多个watch()方法，分别传入待监视的数据例：watch(sum,(n,o)=>{},{});watch(msg,(n,o)=>{},{})
        let sum=ref(0)
        let msg=ref('你好小金鱼')
        // 情况一：监视的属性是一个ref对象
        // 监视一个属性
        // watch(sum,(n,o)=>{
        //     console.log('检测到sum的值改变了,最新的值为'+n+'旧的值为'+o)
        // })
        // watch(msg,(n,o)=>{
        //     console.log('检测到msg的值改变了,最新的值为'+n+'旧的值为'+o)
        // })
   2、写成一个watch()方法，传入一个数组类型的数据，:watch([sum,msg],(n,o)=>{},{})
        let sum=ref(0)
        let msg=ref('你好小金鱼')
        // 监视多个属性
        watch([msg,sum],(n,o)=>{
            console.log('检测到msg的值改变了,最新的值为: '+n[0]+' 旧的值为 :'+o[0])
            console.log('检测到msg的值改变了,最新的值为: '+n[1]+' 旧的值为 :'+o[1])
        })
## 情况二 如果监视的是reactive定义的对象类型的proxy对象
        let person=reactive({
            firstName:'张',
            lastName:'三',
            job:{
                j1:{
                    salay:12
                }
            }
        })
        1、如果监视的是person:则强制深度监视，deep配置项不起任何作用，【不能获取准确的oldValue】
            // 情况一，监视reactive的返回值为person的属性
            // watch(person,(n,o)=>{
            //     console.log('监测到person中属性的变化，最新的值为：'+n.firstName+' 旧的值为： '+o.firstName)
            //     person.fullName=person.firstName+'-'+person.lastName
            // })
        2、如果监视的是person中某个数值类型的值的时候，不能够直接在watch()组合api中，写入person.firstName
        例：watch(person.firstName)--->报错
            应该写成watch(()=>person.firstName,(n,o)=>{})
            或者对于监视多个person中的属性，可以通过watch([()=>person.firstName,()=>person.lastName],(n,o)=>{})
            // 情况二、监视person中的一个或多个数值类型的属性
            // watch([()=>person.age,()=>person.firstName],(n,o)=>{
            //     console.log('监测到person中age属性的变化，最新的值为：'+n[0]+' 旧的值为： '+o[0])
            // })
            // watch(()=>person.age,(n,o)=>{
            //     console.log('监测到person中age属性的变化，最新的值为：'+n[0]+' 旧的值为： '+o[0])
            // })
        3、如果监视person中的对象类型的属性变化，必须要配置deep为true，才能对对象中的属性进行深度监视
            // 情况三:监视person中的一个或多个[对象类型]的属性,必须要写上deep：true，才能对person中的数据进行深度监视
            watch(()=>person.job,(n,o)=>{
                console.log('监测到person中job属性的变化，最新的值为：'+n.j1.salay+' 旧的值为： '+o.j1.salay)
            },{
                deep:true
            })
# 总结
    1、对于数值类型的值的监视，可以得到准确的oldValue值，对于proxy对象的监视不能够得到准确的oldValue的值
    2、如果是对reactive定义的数据进行监视，则开启了强制的深度监视
    3、对proxy对象中的数值型属性进行监视，则必须watch(()=>person.attrs)来对属性进行监视，且监视的的如果是一个proxy的属性，则要设置deep:true才能对person.attrs对象属性开启深度监视
    4、可以对多个属性进行监视watch([person，()=>person.job,····])
    5、watch(被监视的参数)该参数只能是ref引用对象或proxy对象或[ref引用对象或proxy对象],如果是基本数值型的数据的监视只能
    例：
    let sum=ref(0)
    写法1 watch(sum,handler(n,o),{})/watch(()=>sum.value,handler(n,o),{})
    let let person=ref({
            firstName:'张',
            lastName:'三',
            age:14,
            job:{
                j1:{
                    salary:12
                }
            }
        })
    监视job中的属性的变化
    watch(person.job,handler(n,o),{})或者写成watch(()=>person.job,handler(n,o),{deep:true})
    因为只要是通过()=>待监视的额变量，就会当作是数值型的变量，对他的监视，自然不需要deep，