# 异步组件
* 通过引入vue中的defineAsyncComponent函数   import {defineAsyncComponent} from 'vue'
* 生成异步组件 const AsyncComp=defineAsyncComponent(()=>import('组件所在路径'))
* 配置组件 new Vue.extends({components:{AsyncComp}})
* 应用组件<AsyncComp/>
# 异步组件可以配合
        <Suspense> suspense:意为悬而未决的，渲染哪个组件取决于，命名插槽中default的组件是否存在，如果存在则渲染，如果没有则渲染应急插槽中的组件
            <template v-slot:default>
                <AsyncComp>
            </template>
            <template v-slot:[fallback]>
                <h1>组件加载中</h1>
            </template>
        </Suspense>
# 【最重要的一点】
* 同步引入的组件中的setup配置项，【依靠返回值】，来得到响应式的数据和方法，对data、computed、methods进行了封装
由于依靠了返回值中的响应式数据，setup函数不能被async修饰，即不能是一个async函数，