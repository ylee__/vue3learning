# 【回顾】
    school=Vue.extend({       可以简写为school={
        template:`              template:`
            <div></div>              <div></div>
        `,                      `,
        data(){                   data(){
            return {                 return {

            }                         }
        }                          }  
    })                            })

# 【setup配置项代替data\methods\computed等配置项】
    setup作为Vue3的新的配置项，是一个函数，具有返回值，返回的是一个对象，返回的对象中的方法和属性，Vue是可以监视的到的
    【返回的数据是不具有响应式的】
## 虽然setup（）配置项代替了data，但是里面的数据如果直接声明的话，不是响应式的数据
    即数据不是响应式的数据，数据一改，vm检测不到,只能通过引入{ref}来产生ref引用对象
    setup(){
        let name=ref('Jennie')
        let car=ref({
            type:'奔驰',
            price:'13w'
        })
        let job=reactive({
            type:'后端工程师',
            salary:'30w'
        })
        return {
            name,
            job,
            car
        }
    }
    【通过this.name可以直接得到'jennie'而不是refimpl对象】，也就是说vm身上可以直接访问name，
        类似于data(){
            retrun{
                name:'jennie',
                job:{
                    salary:'30w',
                    type:'后端工程师'
                }
            }
        }
        data中的数据同样经过了，数据劫持【_data将data中数据变为响应式的数据】和数据代理【将data中的属性放到vm身上】，
        setup返回的数据（ 从object对象--proxy对象》）
# ref()和reactive()工厂函数
    import {ref,reactive} from 'vue'
    ref(参数可以是基本数据类型，也可以是对象类型)返回值是一个refimpl引用对象,
        value: (...)
        [[Prototype]]: Object
            constructor: class RefImpl
        在setup中访问需要通过car.value是一个proxy实例对象，中的属性是一样的，只不过是响应式的数据
        car.value.type='奔驰'
        name.value是一个字符串
    reactive(参数只能是对象类型)返回值是一个proxy实例对象
        在setup中可以直接访问该对象
        job.type='后端工程师'
# setup()函数的返回值是一个proxy对象，组件实例对象可以直接访问