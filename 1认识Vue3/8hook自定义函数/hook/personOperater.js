import { reactive ,toRefs,toRef} from "vue"
export default function(){
    let person =reactive({
        name:'张三',
        age:14,
        job:{
            j1:{
                type:'写书',
                salary:15
            }
        }
    })
    function personOperater(){
        person.name+='~',
        person.age++
        person.job.j1.salary++
    }
    console.log('toRefs(person)',toRefs(person))
        return {
        person,
        name:toRef(person,'name'),
        age:toRef(person,'age'),
        salary:toRef(person.job.j1,'salary'),
        personOperater
    }
}