## 【新特性】   
        1、拥抱TypeScript（JavaScript With Syntax For Types.带有类型语法的javasript）之前也有，只不过更加
           vue2中对vue的定义是【用于构建用户界面的渐进式（对渐进式的理解就是，vue可以通过使用插件的方式，增强vue功能）js框架】，但是现在可以更好的运行和解析ts代码了
        2、

## 【app代替了vm】
        import {createApp} from 'vue'  createApp是一个通过工厂模式用于生产的对象的函数，相比较于vm对象更加轻量
        import App from 'App'
        const app=createApp(App)
        app.mount('#app')
        setTimeOut(function(){
            app.unmount('#app')
        },1000)
## 【template中可以写多个标签了】
        <template>
            <img alt="Vue logo" src="./assets/logo.png">
            <HelloWorld msg="Welcome to Your Vue.js App"/>
        </template>
## 【vue3是向下兼容的】