# setup()函数什么时候被调用
    比beforeCreate早调用
# setup中的this  
    是undefined
# setup中的参数  (props(proxy对象)，context（object对象）)
    第一个是proxy类型的对象
    该对象就是将props接收到的数据转为了proxy对象，来进行储存 ，虽然接收到了props但是只能读操作 
    第二个是一个普通对象
    里面有attrs，emit，slots属性 分别对应组件实例对象身上的$attrs $emit $slots
    context.attrs是一个proxy实例对象，保存了props本应该接收，但是没有的变量值 【和props配置项冲突，有我没他，有她没我】
    context.slots是一个proxy实例对象，保存了虚拟的dom节点，然后通过插槽转为真实dom
    context.emit,【目前最用用的函数，用来触发事件，并传送数据】，配合emits:['事件名称'，'事件名称'，···]
# setup()配置项是组合api表演的舞台
    ref()
    reactive()
    computed()
    watch()
    watchEffect()
    生命周期钩子
    

    