// createApp是工厂模式的函数可以创建的一个app对象，相比较于vm更轻量一些
import { createApp } from 'vue'
import App from './App.vue'
import { createPinia } from 'pinia'
const app=createApp(App)
const pinia=createPinia()
app.use(pinia).mount('#app')
