# 监测store数据的改变，vuex只能是通过计算属性（具有缓存功能），来监视store中state的变化，
 > 类似于这种：const refData=ref（）refData=store.state.data return{refData}虽然是一个响应式数据，但是冰壶会随着store中数据改变而改变。
 > 只有 调用函数（dom事件）function（）{refData=store.state.data}

 # pinia中虽然state是函数形式，但是依然是可以共享数据的